package com.pharm.test.pharmeasytest.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.j256.ormlite.dao.Dao;
import com.pharm.test.pharmeasytest.entity.Result;

import java.sql.SQLException;

/**
 * Created by Gaurav Nivsarkar on 9/29/15.
 */
public class DBManager {

    private static DbHelper dbHelper;
    private static DBManager dbManager;
    SQLiteDatabase sqLiteDatabase;
    private Context context;

    private Dao<Result,Integer> resultDao;

    private Dao<Result,Integer> getResultDao() throws SQLException{
        if(resultDao==null) resultDao=dbHelper.getDao(Result.class);

        return resultDao;

    }


    private DBManager(Context context) {
        this.context = context.getApplicationContext();

        if(dbHelper==null){
            dbHelper = OpenHelperManager.getHelper(this.context, DbHelper.class);
        }

        if(sqLiteDatabase==null){
            sqLiteDatabase = dbHelper.getWritableDatabase();
        }

    }

    public static DBManager getInstance(Context c){
        if(dbManager==null) dbManager=new DBManager(c);
        return dbManager;
    }


    public DbHelper getDbHelper() {
        return dbHelper;
    }

    public SQLiteDatabase getSqLiteDatabase() {
        return sqLiteDatabase;
    }


    public void insert(Result r){
        try {
                getResultDao().createOrUpdate(r);
        }catch (Exception e){
            e.printStackTrace();
        }
    }
}
