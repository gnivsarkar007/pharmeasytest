package com.pharm.test.pharmeasytest.api.interfaces;

/**
 * Created by Gaurav Nivsarkar on 9/29/15.
 */
public interface ProgressUpdateListener {
    void update(int total,int inserted);
}

