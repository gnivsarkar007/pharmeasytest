package com.pharm.test.pharmeasytest.entity;

import android.database.Cursor;
import android.os.Parcel;
import android.os.Parcelable;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

/**
 * Created by Gaurav Nivsarkar on 9/29/15.
 */
@DatabaseTable
public class Result implements Parcelable {

    @DatabaseField(id = true)
    private Integer id;
    @DatabaseField
    private String hkpDrugCode;
    @DatabaseField
    private Integer mfId;
    @DatabaseField
    private String label;
    @DatabaseField
    private String name;
    @DatabaseField
    private String type;
    @DatabaseField
    private String packSize;
    @DatabaseField
    private String manufacturer;
    @DatabaseField
    private Double uPrice;
    @DatabaseField
    private Double oPrice;
    @DatabaseField
    private Double mrp;
    @DatabaseField
    private Integer su;
//    @DatabaseField
//    private Object slug;
    @DatabaseField
    private String packForm;
    @DatabaseField
    private String form;
//    @DatabaseField
//    private Object imgUrl;
    @DatabaseField
    private Integer uip;
//    @DatabaseField
//    private Object generics;
//    @DatabaseField
//    private Object productsForBrand;
    @DatabaseField
    private Integer discountPerc;
    @DatabaseField
    private String pForm;
    @DatabaseField
    private Boolean available;

    /**
     *
     * @return
     * The id
     */
    public Integer getId() {
        return id;
    }

    /**
     *
     * @param id
     * The id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     *
     * @return
     * The hkpDrugCode
     */
    public String getHkpDrugCode() {
        return hkpDrugCode;
    }

    /**
     *
     * @param hkpDrugCode
     * The hkpDrugCode
     */
    public void setHkpDrugCode(String hkpDrugCode) {
        this.hkpDrugCode = hkpDrugCode;
    }

    /**
     *
     * @return
     * The mfId
     */
    public Integer getMfId() {
        return mfId;
    }

    /**
     *
     * @param mfId
     * The mfId
     */
    public void setMfId(Integer mfId) {
        this.mfId = mfId;
    }

    /**
     *
     * @return
     * The label
     */
    public String getLabel() {
        return label;
    }

    /**
     *
     * @param label
     * The label
     */
    public void setLabel(String label) {
        this.label = label;
    }

    /**
     *
     * @return
     * The name
     */
    public String getName() {
        return name;
    }

    /**
     *
     * @param name
     * The name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     *
     * @return
     * The type
     */
    public String getType() {
        return type;
    }

    /**
     *
     * @param type
     * The type
     */
    public void setType(String type) {
        this.type = type;
    }

    /**
     *
     * @return
     * The packSize
     */
    public String getPackSize() {
        return packSize;
    }

    /**
     *
     * @param packSize
     * The packSize
     */
    public void setPackSize(String packSize) {
        this.packSize = packSize;
    }

    /**
     *
     * @return
     * The manufacturer
     */
    public String getManufacturer() {
        return manufacturer;
    }

    /**
     *
     * @param manufacturer
     * The manufacturer
     */
    public void setManufacturer(String manufacturer) {
        this.manufacturer = manufacturer;
    }

    /**
     *
     * @return
     * The uPrice
     */
    public Double getUPrice() {
        return uPrice;
    }

    /**
     *
     * @param uPrice
     * The uPrice
     */
    public void setUPrice(Double uPrice) {
        this.uPrice = uPrice;
    }

    /**
     *
     * @return
     * The oPrice
     */
    public Double getOPrice() {
        return oPrice;
    }

    /**
     *
     * @param oPrice
     * The oPrice
     */
    public void setOPrice(Double oPrice) {
        this.oPrice = oPrice;
    }

    /**
     *
     * @return
     * The mrp
     */
    public Double getMrp() {
        return mrp;
    }

    /**
     *
     * @param mrp
     * The mrp
     */
    public void setMrp(Double mrp) {
        this.mrp = mrp;
    }

    /**
     *
     * @return
     * The su
     */
    public Integer getSu() {
        return su;
    }

    /**
     *
     * @param su
     * The su
     */
    public void setSu(Integer su) {
        this.su = su;
    }


    /**
     *
     * @return
     * The packForm
     */
    public String getPackForm() {
        return packForm;
    }

    /**
     *
     * @param packForm
     * The packForm
     */
    public void setPackForm(String packForm) {
        this.packForm = packForm;
    }

    /**
     *
     * @return
     * The form
     */
    public String getForm() {
        return form;
    }

    /**
     *
     * @param form
     * The form
     */
    public void setForm(String form) {
        this.form = form;
    }



    /**
     *
     * @return
     * The uip
     */
    public Integer getUip() {
        return uip;
    }

    /**
     *
     * @param uip
     * The uip
     */
    public void setUip(Integer uip) {
        this.uip = uip;
    }


    /**
     *
     * @return
     * The discountPerc
     */
    public Integer getDiscountPerc() {
        return discountPerc;
    }

    /**
     *
     * @param discountPerc
     * The discountPerc
     */
    public void setDiscountPerc(Integer discountPerc) {
        this.discountPerc = discountPerc;
    }

    /**
     *
     * @return
     * The pForm
     */
    public String getPForm() {
        return pForm;
    }

    /**
     *
     * @param pForm
     * The pForm
     */
    public void setPForm(String pForm) {
        this.pForm = pForm;
    }

    /**
     *
     * @return
     * The available
     */
    public Boolean getAvailable() {
        return available;
    }

    /**
     *
     * @param available
     * The available
     */
    public void setAvailable(Boolean available) {
        this.available = available;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(this.id);
        dest.writeString(this.hkpDrugCode);
        dest.writeValue(this.mfId);
        dest.writeString(this.label);
        dest.writeString(this.name);
        dest.writeString(this.type);
        dest.writeString(this.packSize);
        dest.writeString(this.manufacturer);
        dest.writeValue(this.uPrice);
        dest.writeValue(this.oPrice);
        dest.writeValue(this.mrp);
        dest.writeValue(this.su);
//        dest.writeParcelable(this.slug, flags);
        dest.writeString(this.packForm);
        dest.writeString(this.form);
//        dest.writeParcelable(this.imgUrl, flags);
        dest.writeValue(this.uip);
//        dest.writeParcelable(this.generics, flags);
//        dest.writeParcelable(this.productsForBrand, flags);
        dest.writeValue(this.discountPerc);
        dest.writeString(this.pForm);
        dest.writeValue(this.available);
    }

    public Result() {
    }

    protected Result(Parcel in) {
        this.id = (Integer) in.readValue(Integer.class.getClassLoader());
        this.hkpDrugCode = in.readString();
        this.mfId = (Integer) in.readValue(Integer.class.getClassLoader());
        this.label = in.readString();
        this.name = in.readString();
        this.type = in.readString();
        this.packSize = in.readString();
        this.manufacturer = in.readString();
        this.uPrice = (Double) in.readValue(Double.class.getClassLoader());
        this.oPrice = (Double) in.readValue(Double.class.getClassLoader());
        this.mrp = (Double) in.readValue(Double.class.getClassLoader());
        this.su = (Integer) in.readValue(Integer.class.getClassLoader());
        this.packForm = in.readString();
        this.form = in.readString();
        this.uip = (Integer) in.readValue(Integer.class.getClassLoader());
        this.discountPerc = (Integer) in.readValue(Integer.class.getClassLoader());
        this.pForm = in.readString();
        this.available = (Boolean) in.readValue(Boolean.class.getClassLoader());
    }

    public static final Parcelable.Creator<Result> CREATOR = new Parcelable.Creator<Result>() {
        public Result createFromParcel(Parcel source) {
            return new Result(source);
        }

        public Result[] newArray(int size) {
            return new Result[size];
        }
    };

    public static Result getResult(Cursor c){
        Result r=new Result();
        if(c!=null && !c.isClosed()){
            r.setName(c.getString(c.getColumnIndex("name")));
            r.setManufacturer(c.getString(c.getColumnIndex("manufacturer")));
            r.setPackForm(c.getString(c.getColumnIndex("packForm")));
            r.setPackSize(c.getString(c.getColumnIndex("packSize")));
            r.setLabel(c.getString(c.getColumnIndex("label")));
            r.setMrp(c.getDouble(c.getColumnIndex("mrp")));
        }
        return r;
    }
}
