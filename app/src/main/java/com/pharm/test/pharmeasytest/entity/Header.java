package com.pharm.test.pharmeasytest.entity;

/**
 * Created by Gaurav Nivsarkar on 9/29/15.
 */
import java.util.HashMap;
import java.util.Map;
public class Header {

    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
