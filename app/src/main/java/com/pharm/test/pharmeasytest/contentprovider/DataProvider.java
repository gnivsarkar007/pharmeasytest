package com.pharm.test.pharmeasytest.contentprovider;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.util.SparseArray;

import com.pharm.test.pharmeasytest.R;
import com.pharm.test.pharmeasytest.database.DBManager;
import com.pharm.test.pharmeasytest.database.DbHelper;

/**
 * Created by Gaurav Nivsarkar on 9/29/15.
 */
public class DataProvider extends ContentProvider {
    public static final int INVALID_URI = -1;
    private static final UriMatcher uriMatcher;
    // Stores the MIME types served by this provider
    private static final SparseArray<String> mimeTypes;
    DBManager dbMan;
    DbHelper helper;

    static final int QUERRY_ALL=1;
    static{

        uriMatcher=new UriMatcher(0);        // Creates an object that associates content URIs with numeric codes
         /*
         * Sets up an array that maps content URIs to MIME types, via a mapping between the
         * URIs and an integer code. These are custom MIME types that apply to tables and rows
         * in this particular provider.
         */
        mimeTypes = new SparseArray<String>();

        uriMatcher.addURI(DataProviderContract.AUTHORITY,DataProviderContract.TABLE_RESULT,QUERRY_ALL);
        mimeTypes.put(QUERRY_ALL,"vnd.android.cursor.item/vnd." +
                DataProviderContract.AUTHORITY + "." +
                DataProviderContract.TABLE_RESULT);

    }
    @Override
    public boolean onCreate() {
       dbMan=DBManager.getInstance(getContext().getApplicationContext());
        helper=dbMan.getDbHelper();
        return true;
    }

    @Override
    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
        int data=uriMatcher.match(uri);
        SQLiteDatabase db=helper.getReadableDatabase();

        if(data == QUERRY_ALL){
            String query= getContext().getApplicationContext().getString(R.string.result_all_data);
           return db.rawQuery(query,selectionArgs);
        }
        return null;
    }

    @Override
    public String getType(Uri uri) {
        return null;
    }

    @Override
    public Uri insert(Uri uri, ContentValues values) {
        return null;
    }

    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {
        return 0;
    }

    @Override
    public int update(Uri uri, ContentValues values, String selection, String[] selectionArgs) {
        return 0;
    }
}
