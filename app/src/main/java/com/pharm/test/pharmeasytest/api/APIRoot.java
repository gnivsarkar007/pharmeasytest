package com.pharm.test.pharmeasytest.api;

import com.pharm.test.pharmeasytest.BuildConfig;
import com.squareup.okhttp.OkHttpClient;

import retrofit.RestAdapter;
import retrofit.client.OkClient;

/**
 * Created by Gaurav Nivsarkar on 9/29/15.
 */
public class APIRoot {
    public static <S> S createService(Class<S> serviceClass, String baseUrl) {
        OkHttpClient client=new OkHttpClient();
        client.setFollowRedirects(true);

        RestAdapter.Builder builder = new RestAdapter.Builder();
        if(baseUrl!=null)
            builder.setEndpoint(baseUrl);
        builder.setClient(new OkClient(client));

        if(BuildConfig.DEBUG) builder.setLogLevel(RestAdapter.LogLevel.FULL);
//                .setConverter(new JacksonConverter());

        RestAdapter adapter = builder.build();

        return adapter.create(serviceClass);
    }
}
