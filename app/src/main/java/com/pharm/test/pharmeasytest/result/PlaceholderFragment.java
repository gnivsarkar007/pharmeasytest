package com.pharm.test.pharmeasytest.result;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.pharm.test.pharmeasytest.R;
import com.pharm.test.pharmeasytest.entity.Result;
import com.squareup.picasso.Picasso;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by Gaurav Nivsarkar on 9/29/15.
 */
public class PlaceholderFragment extends Fragment {
    /**
     * The fragment argument representing the section number for this
     * fragment.
     */
    private static final String ARG_DATA = "args_data";

    /**
     * Returns a new instance of this fragment for the given section
     * number.
     */
    public static PlaceholderFragment newInstance(Result data) {
        PlaceholderFragment fragment = new PlaceholderFragment();
        Bundle args = new Bundle();
        args.putParcelable(ARG_DATA, data);
        fragment.setArguments(args);
        return fragment;
    }

    public PlaceholderFragment() {
    }

    @Bind(R.id.image)
    ImageView image;
    @Bind(R.id.name)
    TextView name;
    @Bind(R.id.manufacturer)
    TextView manufacturer;
    @Bind(R.id.packform)
    TextView packform;
    @Bind(R.id.packSize)
    TextView packsize;
    @Bind(R.id.label)
    TextView label;
    @Bind(R.id.price)
    TextView priceText;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_main, container, false);
        ButterKnife.bind(this, rootView);
        Result data=getArguments().getParcelable(ARG_DATA);
        name.setText(data.getName());
        manufacturer.setText(data.getManufacturer());
        packform.setText(data.getPackForm());
        packsize.setText(data.getPackSize());
        label.setText(data.getLabel());
        Picasso.with(getActivity()).load("http://apod.nasa.gov/apod/image/1509/LightningEclipse_Hervas_1900.jpg").into(image);
        String price="Rs "+(data.getMrp()==null?0:data.getMrp().toString());
        priceText.setText(price);
        return rootView;
    }
}
