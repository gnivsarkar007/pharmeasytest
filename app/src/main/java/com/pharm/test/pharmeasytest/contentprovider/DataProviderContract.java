package com.pharm.test.pharmeasytest.contentprovider;

import android.net.Uri;
import android.provider.BaseColumns;

import com.pharm.test.pharmeasytest.BuildConfig;

/**
 * Created by Gaurav Nivsarkar on 9/29/15.
 */
public class DataProviderContract implements BaseColumns {
    // The URI scheme used for content URIs
    public static final String SCHEME = "content";
    // The provider's authority
    public static final String AUTHORITY = BuildConfig.APPLICATION_ID;
    /**
     * The DataProvider content URI
     */
    public static final Uri CONTENT_URI = Uri.parse(SCHEME + "://" + AUTHORITY);

    /**
     * The MIME type for a content URI that would return multiple rows
     * <P>Type: TEXT</P> <prefix>://<authority>/<data_type>/<id>
     */
    public static final String MIME_TYPE_ROWS =
            "vnd.android.cursor.dir/vnd.com.pharm.test.pharmeasytest";
    public static final String ROW_ID = BaseColumns._ID;
    public static final String TABLE_RESULT="result";
    public static final Uri CONTENT_URI_RESULTS =
            Uri.withAppendedPath(CONTENT_URI, TABLE_RESULT);
}
