package com.pharm.test.pharmeasytest.result;

import android.app.Fragment;
import android.app.FragmentManager;
import android.database.Cursor;
import android.support.v13.app.FragmentStatePagerAdapter;
import android.util.Log;

import com.pharm.test.pharmeasytest.entity.Result;

/**
 * Created by Gaurav Nivsarkar on 9/29/15.
 */
public class ResultCursorAdapter extends FragmentStatePagerAdapter {
    private Cursor mCursor;
    int oldCursorPosition;
    public ResultCursorAdapter(FragmentManager fm,Cursor c) {
        super(fm);
        this.mCursor =c;
    }

    @Override
    public Fragment getItem(int position) {
        if(mCursor.moveToPosition(position)){
            PlaceholderFragment frag=PlaceholderFragment.newInstance(Result.getResult(mCursor));
            return frag;
        }
        return null;
    }

    @Override
    public int getCount() {
        return mCursor.getCount();
    }

    public Cursor getmCursor() {
        return mCursor;
    }

    public void setmCursor(Cursor mCursor) {
        this.mCursor = mCursor;
    }

    public Cursor swapCursor(Cursor newCursor) {
        Cursor oldCursor = mCursor;
        if (newCursor == mCursor || newCursor == null) {
            if(newCursor==null && oldCursor!=null && !oldCursor.isClosed()) oldCursor.close();
            return null;
        }

//        Toolbox.writeToLog("Old mCursor size was :" + oldCursor.getCount());
        mCursor = newCursor;
        Log.d("Adapter","New mCursor size was :" + mCursor.getCount());

        if (oldCursor.getCount() != mCursor.getCount()) {
            oldCursorPosition = oldCursor.getCount();
            notifyDataSetChanged();
        }

        return oldCursor;
    }
}
