package com.pharm.test.pharmeasytest.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;
import com.pharm.test.pharmeasytest.R;
import com.pharm.test.pharmeasytest.entity.Result;

import java.sql.SQLException;

/**
 * Created by Gaurav Nivsarkar on 9/29/15.
 */
public class DbHelper extends OrmLiteSqliteOpenHelper {

    public static final String DATABASE_NAME = "pharmeasy";
    public static final int DATABASE_VERSION = 3;

    public DbHelper(Context context){
        super(context, DATABASE_NAME,null, DATABASE_VERSION, R.raw.ormlite_config);
    }

    @Override
    public void onCreate(SQLiteDatabase database, ConnectionSource connectionSource) {
       try {
           TableUtils.createTable(connectionSource, Result.class);
       }catch (SQLException e){
           e.printStackTrace();

       }
    }

    @Override
    public void onUpgrade(SQLiteDatabase database, ConnectionSource connectionSource, int oldVersion, int newVersion) {
        try{
            TableUtils.dropTable(connectionSource,Result.class,true);
            onCreate(database,connectionSource);
        }catch (SQLException e){
            e.printStackTrace();
        }
    }
}
