package com.pharm.test.pharmeasytest;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;
import android.widget.Toast;

import com.pharm.test.pharmeasytest.api.APIGetResults;
import com.pharm.test.pharmeasytest.api.interfaces.ProgressUpdateListener;
import com.pharm.test.pharmeasytest.entity.DataResponse;
import com.pharm.test.pharmeasytest.result.MainActivity;

import butterknife.Bind;
import butterknife.ButterKnife;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by Gaurav Nivsarkar on 9/29/15.
 */
public class SplashActivity extends Activity {

    @Bind(R.id.update)
    TextView update;
    private Callback<DataResponse> callback=new Callback<DataResponse>() {
        @Override
        public void success(DataResponse dataResponse, Response response) {
            Intent out=new Intent(SplashActivity.this, MainActivity.class);
            startActivity(out);
            finish();
        }

        @Override
        public void failure(RetrofitError error) {
            Toast.makeText(SplashActivity.this,"Something went wrong "+ error.getKind(),Toast.LENGTH_LONG).show();
            update.setText("OOPS!!!Something went wrong.");
        }
    };
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        ButterKnife.bind(this);
        APIGetResults.getResults(getApplication(), callback,updateListener);
        update.setText("Fetching results...");
    }

    ProgressUpdateListener updateListener=new ProgressUpdateListener() {
        @Override
        public void update(int total, int inserted) {
            update.setText("Inserted "+inserted+" out of "+total);
        }
    };


}
