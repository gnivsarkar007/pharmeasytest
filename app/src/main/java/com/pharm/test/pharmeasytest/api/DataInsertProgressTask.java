package com.pharm.test.pharmeasytest.api;

import android.content.Context;
import android.os.AsyncTask;

import com.pharm.test.pharmeasytest.api.interfaces.ProgressUpdateListener;
import com.pharm.test.pharmeasytest.database.DBManager;
import com.pharm.test.pharmeasytest.entity.DataResponse;
import com.pharm.test.pharmeasytest.entity.Result;

import java.util.ArrayList;
import java.util.List;

import retrofit.Callback;

/**
 * Created by Gaurav Nivsarkar on 9/29/15.
 */
public class DataInsertProgressTask extends AsyncTask<Void,Integer,Void> {

    List<Result> results=new ArrayList<Result>();
    Context context;
    DBManager dbManager;
    DataResponse dataResponse;
    Callback<DataResponse> callback;

    ProgressUpdateListener updateListener;
    public DataInsertProgressTask(DataResponse data,Context c,ProgressUpdateListener p, Callback<DataResponse> callback){
        results.addAll(data.getResult());
        context=c.getApplicationContext();
        updateListener=p;
        dbManager=DBManager.getInstance(c);
        dataResponse=data;
        this.callback=callback;
    }
    @Override
    protected void onProgressUpdate(Integer... values) {
        updateListener.update(results.size(),values[0]);
    }

    @Override
    protected Void doInBackground(Void... params) {
       for(int i=0;i<results.size();i++){
           dbManager.insert(results.get(i));
           if(i%10==0) publishProgress(i);
       }
        publishProgress(results.size());
        return null;
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        callback.success(dataResponse,null);
    }
}
