package com.pharm.test.pharmeasytest.api;

import android.content.Context;

import com.pharm.test.pharmeasytest.api.interfaces.ApiGetResults;
import com.pharm.test.pharmeasytest.api.interfaces.ProgressUpdateListener;
import com.pharm.test.pharmeasytest.entity.DataResponse;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by Gaurav Nivsarkar on 9/29/15.
 */
public class APIGetResults extends APIRoot {

    public static final String BASE_URL=" https://www.1mg.com/api/v1/search";
    public static final String EXTENSION="autocomplete";

    public static void getResults(final Context c,final Callback<DataResponse> listener,final ProgressUpdateListener p){
        ApiGetResults results=createService(ApiGetResults.class,BASE_URL);
        Callback<DataResponse> callback=new Callback<DataResponse>() {
            @Override
            public void success(final DataResponse o, final Response response) {
              DataInsertProgressTask task=new DataInsertProgressTask(o,c,p,listener);
                task.execute();

            }

            @Override
            public void failure(RetrofitError error) {
                if(listener!=null) listener.failure(error);
            }
        };
        results.getResults(EXTENSION,"b","1000000",callback);
    }



}
