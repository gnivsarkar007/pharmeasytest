package com.pharm.test.pharmeasytest.result;

import android.app.Activity;
import android.content.CursorLoader;
import android.content.Loader;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v13.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;

import com.pharm.test.pharmeasytest.R;
import com.pharm.test.pharmeasytest.contentprovider.DataProviderContract;

public class MainActivity extends Activity implements android.app.LoaderManager.LoaderCallbacks<Cursor>{

    public static final int LOADER_ID=101;
    /**
     * The {@link android.support.v4.view.PagerAdapter} that will provide
     * fragments for each of the sections. We use a
     * {@link FragmentPagerAdapter} derivative, which will keep every
     * loaded fragment in memory. If this becomes too memory intensive, it
     * may be best to switch to a
     * {@link android.support.v13.app.FragmentStatePagerAdapter}.
     */
//    SectionsPagerAdapter mSectionsPagerAdapter;

    /**
     * The {@link ViewPager} that will host the section contents.
     */
    ViewPager mViewPager;
    ResultCursorAdapter adapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.
//        mSectionsPagerAdapter = new SectionsPagerAdapter(getFragmentManager());

        // Set up the ViewPager with the sections adapter.
        mViewPager = (ViewPager) findViewById(R.id.pager);
//        mViewPager.setAdapter(mSectionsPagerAdapter);
        initOrRestartLoader();

    }






    private void initOrRestartLoader(){
        if (getLoaderManager().getLoader(LOADER_ID) == null) {
            getLoaderManager().initLoader(LOADER_ID, null, this);
        } else {
            getLoaderManager().restartLoader(LOADER_ID, null, this);
        }
    }


    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        return new CursorLoader(getBaseContext(), DataProviderContract.CONTENT_URI_RESULTS,null,null,null,null);

    }

    @Override
    public void onLoadFinished(android.content.Loader<Cursor> loader, Cursor data) {
           if(data!=null){
               if(adapter==null) {
                   adapter = new ResultCursorAdapter(getFragmentManager(), data);
                    mViewPager.setAdapter(adapter);
               }else{
                   adapter.swapCursor(data);
               }

           }
    }

    @Override
    public void onLoaderReset(android.content.Loader<Cursor> loader) {
       if(adapter!=null)
        adapter.swapCursor(null);
    }
}


