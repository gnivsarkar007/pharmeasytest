package com.pharm.test.pharmeasytest.api.interfaces;


import com.pharm.test.pharmeasytest.entity.DataResponse;

import retrofit.Callback;
import retrofit.http.GET;
import retrofit.http.Path;
import retrofit.http.Query;

/**
 * Created by Gaurav Nivsarkar on 9/29/15.
 */
public interface ApiGetResults {
    @GET("/{path}")
    void getResults(@Path("path")String url, @Query("name") String name,@Query("pageSize") String size,Callback<DataResponse> result);
}
