#
# generated on 2015/09/29 08:07:23
#
# --table-start--
dataClass=com.pharm.test.pharmeasytest.entity.Result
tableName=result
# --table-fields-start--
# --field-start--
fieldName=id
id=true
# --field-end--
# --field-start--
fieldName=hkpDrugCode
# --field-end--
# --field-start--
fieldName=mfId
# --field-end--
# --field-start--
fieldName=label
# --field-end--
# --field-start--
fieldName=name
# --field-end--
# --field-start--
fieldName=type
# --field-end--
# --field-start--
fieldName=packSize
# --field-end--
# --field-start--
fieldName=manufacturer
# --field-end--
# --field-start--
fieldName=uPrice
# --field-end--
# --field-start--
fieldName=oPrice
# --field-end--
# --field-start--
fieldName=mrp
# --field-end--
# --field-start--
fieldName=su
# --field-end--
# --field-start--
fieldName=packForm
# --field-end--
# --field-start--
fieldName=form
# --field-end--
# --field-start--
fieldName=uip
# --field-end--
# --field-start--
fieldName=discountPerc
# --field-end--
# --field-start--
fieldName=pForm
# --field-end--
# --field-start--
fieldName=available
# --field-end--
# --table-fields-end--
# --table-end--
#################################
